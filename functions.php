<?php
    // Create the menus
    function agus_menu() {
        
        // Wordpress function
        register_nav_menus( array (
            'main-menu' => 'Main Menu'
        ) );
    }

    // Support Custom Logo's
    function themename_custom_logo_setup() {
        $defaults = array(
            'header-text'          => array( 'site-title', 'site-description' ),
            'height'               => 100,
            'width'                => 400,
            'flex-height'          => true,
            'flex-width'           => true
        );

        add_theme_support( 'custom-logo', $defaults );
    }

    add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

    add_theme_support( 'title-tag' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( "wp-block-styles" );
    add_theme_support( "responsive-embeds" );
    add_theme_support( "html5" );
    add_theme_support( "custom-header" );
    add_theme_support( "custom-background" );
    add_theme_support( "align-wide" );
    // Function Hook
    add_action( 'init', 'agus_menu' );

    // Additional Class for main-menu
    function add_menu_list_item_class($classes, $item, $args) {
        if (property_exists($args, 'list_item_class')) {
            $classes[] = $args->list_item_class;
        }
        return $classes;
    }
    add_filter('nav_menu_css_class', 'add_menu_list_item_class', 1, 3);

    function add_menu_link_class( $atts, $item, $args ) {
        if (property_exists($args, 'link_class')) {
            $atts['class'] = $args->link_class;
        }
        return $atts;
    }
    add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );

    // Load CSS and JS Files
    function agus_scripts() {
        // Floating Button CSS

        // Bootstrap Library
        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '5.1.1');
        wp_register_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array(), '5.1.1', true);
        wp_enqueue_script('bootstrapjs');

        // Main CSS
        wp_enqueue_style('style', get_stylesheet_uri(), array('bootstrap'), '1.0.0' );
    }
    add_action( 'wp_enqueue_scripts', 'agus_scripts' );

    // Widgets
    function agus_widgets() {
        register_sidebar( array(
            'name' => 'Footer #1',
            'id' => 'footer1',
            'before_widget' => '<div id="%1$s" class="%2$s footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ) );

        register_sidebar( array(
            'name' => 'Footer #2',
            'id' => 'footer2',
            'before_widget' => '<div id="%1$s" class="%2$s footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ) );

        register_sidebar( array(
            'name' => 'Footer #3',
            'id' => 'footer3',
            'before_widget' => '<div id="%1$s" class="%2$s footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ) );

        register_sidebar( array(
            'name' => 'Footer #4',
            'id' => 'footer4',
            'before_widget' => '<div id="%1$s" class="%2$s footer-menu">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ) );
    }
    add_action( 'widgets_init', 'agus_widgets' );


    add_shortcode( 'custom_element', 'custom_func' );
    function custom_func( $atts, $content = null ) { // New function parameter $content is added!
        return "<div style='position: fixed;overflow: hidden;top: -100%;' >".$atts['add_keywords']."</div>";
    }

    add_action( 'vc_before_init', 'your_name_integrateWithVC' );
    function your_name_integrateWithVC() {
        vc_map( array(
        "name" => __( "Custom Element", "my-text-domain" ),
        "base" => "custom_element",
        "class" => "",
        "category" => __( "Custom", "my-text-domain"),
        "params" => array(
        array(
        "type" => "textfield",
        "holder" => "div",
        "class" => "",
        "heading" => __( "Add Keywords", "my-text-domain" ),
        "param_name" => "add_keywords",
        "value" => __( "", "my-text-domain" ),
        "description" => __( "to add keywords for your SEO", "my-text-domain" )
        )
        )
        ) );
    }