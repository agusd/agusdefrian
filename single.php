<?php get_header(); ?>

<?php while(have_posts() ): the_post(); ?>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <main class="container pt-5 pb-5">
        <?php the_content(); ?>

        <?php 
            $args = array(
                'before' => '<p>' . __( 'Pages', 'agus-defrian' ),
                'after' => '</p>'
            );
            wp_link_pages($args); 
        ?>
    </main>
</section>
<?php endwhile; ?>

<?php get_footer(); ?>