<?php get_header(); ?>

<?php while(have_posts() ): the_post(); ?>
<section>
    <main class="container pt-5 pb-5">
        <?php the_content(); ?>
    </main>
</section>
<?php endwhile; ?>

<?php get_footer(); ?>