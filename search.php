<?php get_header(); ?>
<section>
    <main class="container pb-5 pt-5">
        <h2>Search Result : <?php the_search_query(); ?></h2>
        <div class="row">
            <?php while(have_posts() ): the_post(); ?>
                <div class="col-auto pt-5">
                    <div class="card" style="width: 18rem;">
                    <?php if (has_post_thumbnail( $post->ID ) ) : ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                            <img src="<?php echo $image[0]; ?>" class="card-img-top card-image" alt="...">
                        <?php else: ?>
                            <img src="<?php echo esc_url(get_template_directory_uri() . "/img/no-image.jpg"); ?>" class="card-img-top card-image" alt="...">
                    <?php endif; ?>
                        <div class="card-body">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                            <p class="card-title">By : <?php echo get_the_author_meta('display_name'); ?></p>
                            <p class="card-text"><?php echo get_the_archive_description(); ?></p>
                            <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read</a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <div class="row">
            <div class="col-12 pt-5 text-center">
                <h5><?php echo paginate_links(); ?><h5>
            </div>
        </div>
    </main>
</section>
<?php get_footer(); ?>
