<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div> Test </div>
    <?php wp_body_open(); ?>
    <div> Hello World! </div>
    <header class="site-header">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container header-content">
                <a class="navbar-brand" href="<?php echo esc_url(home_url()); ?>">
                    <?php
                        $custom_logo_id = get_theme_mod( 'custom_logo' );
                        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

                        if ( has_custom_logo() ) {
                            echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '">';
                        } else {
                            echo '<h1>' . get_bloginfo('name') . '</h1>';
                        }
                    ?>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div id="navbarNav" class="collapse navbar-collapse">
                    <?php
                        $args = array(
                            'container' => 'div',
                            'container_class' => 'pe-4 ms-auto',
                            'menu_class' => 'navbar-nav',
                            'list_item_class'  => 'nav-item',
                            'link_class'   => 'nav-link'
                        );
                        wp_nav_menu($args);
                    ?>
                    <?php get_search_form(); ?>
                </div>
            </div>
        </nav>
    </header>