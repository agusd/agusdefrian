    <footer class="site-footer text-light text-lg-start">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row">
        <!--Grid column-->
        <?php if (is_active_sidebar('footer1')) : ?>
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <?php dynamic_sidebar('footer1'); ?>
            </div>
        <?php endif; ?>
        <!--Grid column-->
        <?php if (is_active_sidebar('footer2')) : ?>
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <?php dynamic_sidebar('footer2'); ?>
            </div>
        <?php endif; ?>
        <!--Grid column-->
        <?php if (is_active_sidebar('footer3')) : ?>
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <?php dynamic_sidebar('footer3'); ?>
            </div>
        <?php endif; ?>
        <!--Grid column-->
        <?php if (is_active_sidebar('footer4')) : ?>
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <?php dynamic_sidebar('footer4'); ?>
            </div>
        <?php endif; ?>
        <!--Grid column-->
        </div>
        <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: #16191b;">
        Copyright &copy; <?php echo date('Y'); ?> <?php echo get_bloginfo('name'); ?>
    </div>
    <!-- Copyright -->
    </footer>
<?php wp_footer(); ?>
</body>
</html>
