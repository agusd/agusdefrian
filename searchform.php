<form role="search" method="get" id="searchform" action="<?php echo esc_url(home_url( '/' )); ?>" class="d-flex">
    <input name="s" id="s" class="form-control me-2" type="search" placeholder="Search" aria-label="Search" value="<?php echo get_search_query(); ?>" required />
    <button class="btn btn-outline-primary" type="submit">Search</button>
</form>